package io.peg.dp.composite;

import io.peg.dp.composite.component.MenuComponent;

public class Waitress {
    private MenuComponent rootComponent;

    public Waitress(MenuComponent rootComponent) {
        this.rootComponent = rootComponent;
    }

    public void printMenu() {
        this.rootComponent.print();
    }
}
