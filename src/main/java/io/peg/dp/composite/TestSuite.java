package io.peg.dp.composite;

import io.peg.dp.composite.component.Menu;
import io.peg.dp.composite.component.MenuComponent;
import io.peg.dp.composite.component.MenuItem;

public class TestSuite {
    public static void main(String[] args) {
        MenuComponent pancakeHouseMenu = new Menu("PANCAKE HOUSE MENU", "Breakfast");
        MenuComponent dinerMenu = new Menu("DINER MENU", "Lunch");
        MenuComponent cafeMenu = new Menu("CAFE MENU", "Dinner");
        MenuComponent dessertMenu = new Menu("DESSERT MENU", "Dessert of course");

        MenuComponent rootMenu = new Menu("ROOT MENU", "All menu combined");

        rootMenu.add(pancakeHouseMenu);
        rootMenu.add(dinerMenu);
        rootMenu.add(cafeMenu);

        dinerMenu.add(new MenuItem("Pasta", "Spaghetti with Marinara Sauce, and a slice of sourdough bread", true, 3.89f));
        dinerMenu.add(dessertMenu);

        dessertMenu.add(new MenuItem("Apple Pie", "Apple pie with a flakey crust, topped with vanilla ice cream", true, 1.59f));

        Waitress waitress = new Waitress(rootMenu);
        waitress.printMenu();
    }
}
