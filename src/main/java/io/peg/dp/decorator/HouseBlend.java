package io.peg.dp.decorator;

/**
 * 综合咖啡
 */
public class HouseBlend extends Beverage {
    public HouseBlend() {
        this.description = "HouseBlend";
    }

    public float cost() {
        return .89f;
    }
}
