package io.peg.dp.decorator;

public abstract class Beverage {
    protected String description;

    public abstract float cost();

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return this.getDescription() + ": " + this.cost();
    }
}
