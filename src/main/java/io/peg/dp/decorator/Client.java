package io.peg.dp.decorator;

import io.peg.dp.decorator.condiment.Mocha;
import io.peg.dp.decorator.condiment.Whip;

public class Client {
    public static void main(String[] args) {
        Beverage espresso = new Espresso();
        Beverage houseblend = new HouseBlend();

        System.out.println(new Whip(new Mocha(new Mocha(espresso))));
        System.out.println(new Mocha(houseblend));
    }
}
