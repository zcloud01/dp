package io.peg.dp.decorator;

/**
 * 浓缩咖啡
 */
public class Espresso extends Beverage {
    public Espresso() {
        this.description = "Espresso";
    }

    public float cost() {
        return 1.99f;
    }
}
