package io.peg.dp.decorator.condiment;

import io.peg.dp.decorator.Beverage;

public abstract class CondimentDecorator extends Beverage {
    protected Beverage beverage;
    public abstract String getDescription();
}
