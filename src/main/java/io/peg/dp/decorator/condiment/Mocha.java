package io.peg.dp.decorator.condiment;

import io.peg.dp.decorator.Beverage;
import io.peg.dp.decorator.condiment.CondimentDecorator;

public class Mocha extends CondimentDecorator {

    public Mocha(Beverage beverage) {
        this.beverage = beverage;
    }

    public float cost() {
        return .20f + this.beverage.cost();
    }

    public String getDescription() {
        return this.beverage.getDescription() + ", Mocha";
    }
}
