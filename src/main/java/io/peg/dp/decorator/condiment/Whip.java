package io.peg.dp.decorator.condiment;

import io.peg.dp.decorator.Beverage;

public class Whip extends CondimentDecorator {

    public Whip(Beverage beverage) {
        this.beverage = beverage;
    }

    public float cost() {
        return .10f + this.beverage.cost();
    }

    public String getDescription() {
        return this.beverage.getDescription() + ", whip";
    }
}
