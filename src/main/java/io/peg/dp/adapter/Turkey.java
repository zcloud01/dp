package io.peg.dp.adapter;

public interface Turkey {
    void gobble();
    void fly();
}
