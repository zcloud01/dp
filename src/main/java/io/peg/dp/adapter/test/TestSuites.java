package io.peg.dp.adapter.test;

import io.peg.dp.adapter.Duck;
import io.peg.dp.adapter.MallardDuck;
import io.peg.dp.adapter.TurkeyAdapter;
import io.peg.dp.adapter.WildTurkey;
import org.junit.Test;

public class TestSuites {

    @Test
    public void test() {
        Duck mallardDuck = new MallardDuck();

        System.out.println("The duck says...");

        mallardDuck.quack();
        mallardDuck.fly();

        WildTurkey turkey = new WildTurkey();
        Duck turkeyAdapter = new TurkeyAdapter(turkey);

        System.out.println("The turkey says...");
        turkey.gobble();
        turkey.fly();

        System.out.println("The turkeyAdapter says...");
        turkeyAdapter.quack();
        turkeyAdapter.fly();
    }
}
