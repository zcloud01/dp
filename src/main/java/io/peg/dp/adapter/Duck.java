package io.peg.dp.adapter;

public interface Duck {
    void quack();
    void fly();
}
