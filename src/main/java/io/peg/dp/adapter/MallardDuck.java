package io.peg.dp.adapter;

public class MallardDuck implements Duck {
    public void quack() {
        System.out.println("quack...");
    }

    public void fly() {
        System.out.println("fly...");
    }
}
