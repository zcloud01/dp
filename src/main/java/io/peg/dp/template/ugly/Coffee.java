package io.peg.dp.template.ugly;

public class Coffee {
    public void prepareRecipe() {
        this.boilWater();
        this.brewCoffeeGrinds();
        this.pourInCup();
        this.addSugarAndMilk();
    }

    private void addSugarAndMilk() {
        System.out.println("add sugar and milk...");
    }

    private void pourInCup() {
        System.out.println("pour in cup...");
    }

    private void brewCoffeeGrinds() {
        System.out.println("brew coffee grinds...");
    }

    private void boilWater() {
        System.out.println("boil Water....");
    }
}
