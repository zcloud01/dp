package io.peg.dp.template.ugly;

public class Tea {
    public void prepareRecipe() {
        this.boilWater();
        this.steepTeaBag();
        this.pourInCup();
        this.addLemon();
    }

    private void addLemon() {
        System.out.println("add lemon...");
    }

    private void pourInCup() {
        System.out.println("pour in cup...");
    }

    private void steepTeaBag() {
        System.out.println("steep tea bag...");
    }

    private void boilWater() {
        System.out.println("boil water...");
    }
}
