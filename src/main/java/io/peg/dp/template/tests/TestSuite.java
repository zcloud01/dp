package io.peg.dp.template.tests;

import io.peg.dp.template.book.Beverage;
import io.peg.dp.template.book.Coffee;
import io.peg.dp.template.book.CoffeeWithHook;
import io.peg.dp.template.book.Tea;

public class TestSuite {

    public static void main(String[] args) {
        Beverage coffee = new Coffee();
        coffee.prepareRecipe();
        Beverage tea = new Tea();
        tea.prepareRecipe();

        Beverage coffeeWithHook = new CoffeeWithHook();
        coffeeWithHook.prepareRecipe();
    }
}
