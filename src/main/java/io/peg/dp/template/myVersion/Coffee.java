package io.peg.dp.template.myVersion;

public class Coffee extends Beverage {
    public void prepareRecipe() {
        this.boilWater();
        this.brewCoffeeGrinds();
        this.pourInCup();
        this.addSugarAndMilk();
    }

    private void addSugarAndMilk() {
        System.out.println("add sugar and milk...");
    }

    private void brewCoffeeGrinds() {
        System.out.println("brew coffee grinds...");
    }
}
