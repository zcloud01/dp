package io.peg.dp.template.myVersion;

public class Tea extends Beverage {
    public void prepareRecipe() {
        this.boilWater();
        this.steepTeaBag();
        this.pourInCup();
        this.addLemon();
    }

    private void addLemon() {
        System.out.println("add lemon...");
    }

    private void steepTeaBag() {
        System.out.println("steep tea bag...");
    }
}
