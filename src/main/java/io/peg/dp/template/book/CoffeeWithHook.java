package io.peg.dp.template.book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CoffeeWithHook extends Beverage {

    public void addCondiments() {
        System.out.println("add sugar and milk...");
    }

    public void brew() {
        System.out.println("brew coffee grinds...");
    }

    @Override
    public boolean customerWantsCondiments() {
        String answer = getUserInput();

        if (answer.toLowerCase().startsWith("y")) {
            return true;
        } else {
            return false;
        }
    }

    private String getUserInput() {
        String answer = null;
        System.out.println("Would you like milk and sugar with your coffee (y/n) ?");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        try {
            answer = reader.readLine();
        } catch (IOException e) {
            System.out.println(e);
        }
        if (answer == null) {
            return "no";
        }

        return answer;
    }
}
