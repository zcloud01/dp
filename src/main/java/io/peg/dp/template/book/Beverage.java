package io.peg.dp.template.book;

public abstract class Beverage {

    public void prepareRecipe() {
        this.boilWater();
        this.brew();
        this.pourInCup();
        if (customerWantsCondiments()) {
            this.addCondiments();
        }
    }

    public abstract void addCondiments();

    public abstract void brew();

    public void boilWater() {
        System.out.println("boil water...");
    }

    public void pourInCup() {
        System.out.println("pour in cup...");
    }

    public boolean customerWantsCondiments() {
        return true;
    }
}
