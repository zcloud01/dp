package io.peg.dp.template.book;

public class Coffee extends Beverage {

    @Override
    public void addCondiments() {
        System.out.println("add sugar and milk...");
    }

    @Override
    public void brew() {
        System.out.println("brew coffee grinds...");
    }
}
