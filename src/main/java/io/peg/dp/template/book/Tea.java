package io.peg.dp.template.book;

public class Tea extends Beverage {

    @Override
    public void addCondiments() {
        System.out.println("add lemon...");
    }

    @Override
    public void brew() {
        System.out.println("steep tea bag...");
    }
}
