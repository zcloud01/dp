package io.peg.dp.compound.composite;

import io.peg.dp.compound.duck.Quackable;

import java.util.ArrayList;
import java.util.Iterator;

public class Flock implements Quackable {
    private ArrayList<Quackable> quackables = new ArrayList<Quackable>();

    public void add(Quackable quackable) {
        this.quackables.add(quackable);
    }

    public void quack() {
        Iterator<Quackable> iterator = quackables.iterator();
        while (iterator.hasNext()) {
            Quackable quackable = iterator.next();
            quackable.quack();
        }
    }
}
