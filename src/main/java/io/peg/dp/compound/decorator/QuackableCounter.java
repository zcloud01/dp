package io.peg.dp.compound.decorator;

import io.peg.dp.compound.duck.Quackable;

public class QuackableCounter implements Quackable {
    private Quackable quackable;
    private static int numberOfQuacks;

    public QuackableCounter(Quackable quackable) {
        this.quackable = quackable;
        numberOfQuacks++;
    }

    public void quack() {
        this.quackable.quack();
    }

    public static int getNumberOfQuacks() {
        return numberOfQuacks;
    }
}
