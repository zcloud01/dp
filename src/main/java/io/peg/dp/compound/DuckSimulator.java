package io.peg.dp.compound;

import io.peg.dp.compound.adapter.GooseAdapter;
import io.peg.dp.compound.decorator.QuackableCounter;
import io.peg.dp.compound.duck.Goose;
import io.peg.dp.compound.duck.Quackable;
import io.peg.dp.compound.factory.AbstractDuckFactory;
import io.peg.dp.compound.factory.CountingDuckFactory;

public class DuckSimulator {
    public static void main(String[] args) {
        DuckSimulator simulator = new DuckSimulator();
        simulator.simulate();
    }

    public void simulate() {
        AbstractDuckFactory duckFactory = new CountingDuckFactory();
        Quackable mallardDuck = duckFactory.createMallardDuck();
        Quackable redheadDuck = duckFactory.createRedheadDuck();
        Quackable rubberDuck = duckFactory.createRubberDuck();
        Quackable duckCall = duckFactory.createDuckCall();
        Quackable goose = new QuackableCounter(new GooseAdapter(new Goose()));

        System.out.println("Duck Simulator");
        simulate(mallardDuck);
        simulate(redheadDuck);
        simulate(rubberDuck);
        simulate(duckCall);
        simulate(goose);
        System.out.println("Number of quacks: " + QuackableCounter.getNumberOfQuacks());
    }

    private void simulate(Quackable quackable) {
        quackable.quack();
    }
}