package io.peg.dp.compound.duck;

public class MallardDuck implements Quackable {
    public void quack() {
        System.out.println("mallard duck quack...");
    }
}
