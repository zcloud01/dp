package io.peg.dp.compound.duck;

public class DuckCall implements Quackable {
    public void quack() {
        System.out.println("Duck call quack...");
    }
}
