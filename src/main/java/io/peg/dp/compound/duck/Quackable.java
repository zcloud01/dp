package io.peg.dp.compound.duck;

public interface Quackable {
    void quack();
}
