package io.peg.dp.compound.duck;

public class RedheadDuck implements Quackable {
    public void quack() {
        System.out.println("Redhead duck quack...");
    }
}
