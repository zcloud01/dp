package io.peg.dp.compound.duck;

public class RubberDuck implements Quackable {
    public void quack() {
        System.out.println("Rubber duck quack...");
    }
}
