package io.peg.dp.compound.adapter;

import io.peg.dp.compound.duck.Goose;
import io.peg.dp.compound.duck.Quackable;

public class GooseAdapter implements Quackable {
    private Goose goose;

    public GooseAdapter(Goose goose) {
        this.goose = goose;
    }

    public void quack() {
        this.goose.honk();
    }
}
