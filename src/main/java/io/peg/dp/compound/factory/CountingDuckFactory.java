package io.peg.dp.compound.factory;

import io.peg.dp.compound.decorator.QuackableCounter;
import io.peg.dp.compound.duck.*;

public class CountingDuckFactory extends AbstractDuckFactory {
    public Quackable createMallardDuck() {
        return new QuackableCounter(new MallardDuck());
    }

    public Quackable createRedheadDuck() {
        return new QuackableCounter(new RedheadDuck());
    }

    public Quackable createDuckCall() {
        return new QuackableCounter(new DuckCall());
    }

    public Quackable createRubberDuck() {
        return new QuackableCounter(new RubberDuck());
    }
}
