package io.peg.dp.command.test;

import io.peg.dp.command.SimpleRemoteControl;
import io.peg.dp.command.command.LightOnCommand;
import io.peg.dp.command.device.Light;

public class SimpleRemoteControlTest {
    public static void main(String[] args) {
        SimpleRemoteControl control = new SimpleRemoteControl();
        Light light = new Light();
        LightOnCommand lightOnCommand = new LightOnCommand(light);

        control.setCommand(lightOnCommand);
        control.buttonWasPressed();
    }
}
