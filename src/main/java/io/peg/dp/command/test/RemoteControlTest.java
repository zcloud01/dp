package io.peg.dp.command.test;

import io.peg.dp.command.RemoteControl;
import io.peg.dp.command.command.*;
import io.peg.dp.command.device.CeilingFan;
import io.peg.dp.command.device.GarageDoor;
import io.peg.dp.command.device.Light;
import org.junit.Before;
import org.junit.Test;

public class RemoteControlTest {
    private RemoteControl control;
    private Light light;
    private LightOnCommand lightOnCommand;
    private LightOffCommand lightOffCommand;
    private GarageDoor door;
    private GarageDoorCloseCommand garageDoorCloseCommand;
    private GarageDoorOpenCommand garageDoorOpenCommand;
    private CeilingFan ceilingFan;
    private CeilingFanHighCommand ceilingFanHighCommand;
    private CeilingFanMediumCommand ceilingFanMediumCommand;
    private CeilingFanLowCommand ceilingFanLowCommand;
    private CeilingFanOffCommand ceilingFanOffCommand;

    @Before
    public void setup() {
        System.out.println("setup...");
        this.control = new RemoteControl();

        this.light = new Light();
        this.lightOnCommand = new LightOnCommand(this.light);
        this.lightOffCommand = new LightOffCommand(this.light);

        this.door = new GarageDoor();
        this.garageDoorCloseCommand = new GarageDoorCloseCommand(this.door);
        this.garageDoorOpenCommand = new GarageDoorOpenCommand(this.door);

        this.ceilingFan = new CeilingFan("Living Room");
        this.ceilingFanHighCommand  = new CeilingFanHighCommand(this.ceilingFan);
        this.ceilingFanMediumCommand  = new CeilingFanMediumCommand(this.ceilingFan);
        this.ceilingFanLowCommand  = new CeilingFanLowCommand(this.ceilingFan);
        this.ceilingFanOffCommand  = new CeilingFanOffCommand(this.ceilingFan);
    }

    @Test
    public void simpleTest() {
        control.setCommand(0, lightOnCommand, lightOffCommand);
        control.onButtonWasPressed(0);
        control.offButtonWasPressed(0);
        control.setCommand(1, garageDoorOpenCommand, garageDoorCloseCommand);
        control.onButtonWasPressed(1);
        control.offButtonWasPressed(1);
    }

    @Test
    public void undoTest() {
        control.setCommand(0, lightOnCommand, lightOffCommand);
        control.onButtonWasPressed(0);
        control.offButtonWasPressed(0);
        control.undoButtonWasPressed();
    }

    @Test
    public void testCeilingFan() {
        control.setCommand(0, ceilingFanMediumCommand, ceilingFanOffCommand);
        control.setCommand(1, ceilingFanHighCommand, ceilingFanOffCommand);

        control.onButtonWasPressed(0);
        control.offButtonWasPressed(0);
        control.undoButtonWasPressed();

        control.onButtonWasPressed(1);
        control.undoButtonWasPressed();
    }

    @Test
    public void testMacroCommand() {
        Command[] onCommands = {garageDoorOpenCommand, lightOnCommand, ceilingFanLowCommand};
        Command[] offCommands = {garageDoorCloseCommand, lightOffCommand, ceilingFanOffCommand};

        MacroCommand onCommand = new MacroCommand(onCommands);
        MacroCommand offCommand = new MacroCommand(offCommands);

        control.setCommand(0, onCommand, offCommand);
        control.onButtonWasPressed(0);
        control.offButtonWasPressed(0);

        control.undoButtonWasPressed();
    }
}
