package io.peg.dp.command.command;

import io.peg.dp.command.device.Light;

public class LightOnCommand implements Command {
    Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.on();
    }

    public void undo() {
        this.light.off();
    }
}
