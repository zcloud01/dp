package io.peg.dp.command.command;

public interface Command {
    void execute();
    void undo();
}
