package io.peg.dp.command.command;

import io.peg.dp.command.device.GarageDoor;

public class GarageDoorOpenCommand implements Command {
    GarageDoor garageDoor;

    public GarageDoorOpenCommand(GarageDoor garageDoor) {
        this.garageDoor = garageDoor;
    }

    public void execute() {
        this.garageDoor.open();
    }

    public void undo() {
        this.garageDoor.close();
    }
}
