package io.peg.dp.command.command;

import io.peg.dp.command.device.GarageDoor;

public class GarageDoorCloseCommand implements Command {
    private GarageDoor garageDoor;

    public GarageDoorCloseCommand(GarageDoor garageDoor) {
        this.garageDoor = garageDoor;
    }

    public void execute() {
        this.garageDoor.close();
    }

    public void undo() {
        this.garageDoor.open();
    }
}
