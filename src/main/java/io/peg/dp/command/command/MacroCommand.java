package io.peg.dp.command.command;

import java.util.Stack;

public class MacroCommand implements Command {
    private Command[] commands;
    private Stack<Command> undoCommands;

    public MacroCommand(Command[] commands) {
        this.commands = commands;
        this.undoCommands = new Stack<Command>();
    }

    public void execute() {
        for (int i = 0; i < commands.length; i++) {
            this.undoCommands.push(this.commands[i]);
            this.commands[i].execute();
        }
    }

    public void undo() {
        while (!this.undoCommands.isEmpty()) {
            Command command = this.undoCommands.pop();
            command.undo();
        }
    }
}
