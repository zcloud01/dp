package io.peg.dp.command;

import io.peg.dp.command.command.Command;
import io.peg.dp.command.command.NoCommand;

public class RemoteControl {
    private Command[] onCommands = new Command[7];
    private Command[] offCommands = new Command[7];
    private Command undoCommand = new NoCommand();

    public RemoteControl() {
        for (int i = 0; i < onCommands.length; i++) {
            this.onCommands[i] = new NoCommand();
        }
        for (int i = 0; i < offCommands.length; i++) {
            this.offCommands[i] = new NoCommand();
        }
    }

    public void setCommand(int slot, Command onCommand, Command offCommand) {
        this.onCommands[slot] = onCommand;
        this.offCommands[slot] = offCommand;
    }

    public void onButtonWasPressed(int slot) {
        this.onCommands[slot].execute();
        this.undoCommand = this.onCommands[slot];
    }

    public void offButtonWasPressed(int slot) {
        this.offCommands[slot].execute();
        this.undoCommand = this.offCommands[slot];
    }

    public void undoButtonWasPressed() {
        this.undoCommand.undo();
    }
}
