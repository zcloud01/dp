package io.peg.dp.command.device;

public class GarageDoor {
    public void open() {
        System.out.println("Garage Door is open...");
    }

    public void close() {
        System.out.println("Garage Door is close...");
    }
}
