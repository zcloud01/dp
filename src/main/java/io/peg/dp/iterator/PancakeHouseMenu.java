package io.peg.dp.iterator;

import java.util.ArrayList;

/**
 * implements with array
 */
public class PancakeHouseMenu {
    private ArrayList<MenuItem> menuItems;

    public PancakeHouseMenu() {
        this.menuItems = new ArrayList<MenuItem>();
    }

    public void addItem(String name, String description, boolean vegerarian, double price) {
        MenuItem menuItem = new MenuItem(name, description, vegerarian, price);
        this.menuItems.add(menuItem);
    }

    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }
}
