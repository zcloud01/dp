package io.peg.dp.singleton.simple;

/**
 * Created by atom on 2018/11/10.
 */
public class Singleton {
    private static Singleton instance = new Singleton();
    {
        System.out.println("area");
    }

    private Singleton() {
        System.out.println("__init__");
    }

    public static Singleton getInstance() {
        return instance;
    }
}
