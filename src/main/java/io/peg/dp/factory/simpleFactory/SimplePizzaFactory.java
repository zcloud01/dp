package io.peg.dp.factory.simpleFactory;

import io.peg.dp.factory.base.pizza.CheesePizza;
import io.peg.dp.factory.base.pizza.GreekPizza;
import io.peg.dp.factory.base.pizza.PepperoniPizza;
import io.peg.dp.factory.base.pizza.Pizza;

public class SimplePizzaFactory {
    public Pizza createPizza(String type) {
        Pizza pizza = null;

        if (type.equals("cheese")) {
            pizza =  new CheesePizza();
        } else if(type.equals("greek")) {
            pizza = new GreekPizza();
        } else if (type.equals("peppperoni")) {
            pizza = new PepperoniPizza();
        }
        return pizza;
    }
}
