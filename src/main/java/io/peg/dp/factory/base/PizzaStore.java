package io.peg.dp.factory.base;

import io.peg.dp.factory.base.pizza.CheesePizza;
import io.peg.dp.factory.base.pizza.GreekPizza;
import io.peg.dp.factory.base.pizza.PepperoniPizza;
import io.peg.dp.factory.base.pizza.Pizza;

/**
 * Created by atom on 2018/11/10.
 */
public class PizzaStore {
    Pizza orderPizza(String type) {
        System.out.println("order pizza: " + type);

        Pizza pizza = null;

        if (type.equals("cheese")) {
            pizza =  new CheesePizza();
        } else if(type.equals("greek")) {
            pizza = new GreekPizza();
        } else if (type.equals("peppperoni")) {
            pizza = new PepperoniPizza();
        }

        if (pizza != null) {
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();
        }

        return pizza;
    }
}
