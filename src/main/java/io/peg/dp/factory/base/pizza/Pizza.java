package io.peg.dp.factory.base.pizza;

/**
 * Created by atom on 2018/11/10.
 */
public class Pizza {
    public void prepare() {
        System.out.println("prepare...");
    }

    public void bake() {
        System.out.println("bake...");
    }

    public void cut() {
        System.out.println("cut...");
    }

    public void box() {
        System.out.println("box...");
    }
}
