package io.peg.dp.factory.abs.store;

import io.peg.dp.factory.abs.pizza.NYStyleCheesePizza;
import io.peg.dp.factory.abs.pizza.Pizza;

/**
 * Created by atom on 2018/11/10.
 */
public class NYPizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(String type) {
        Pizza pizza = null;

        if (type.equals("cheese")) {
            pizza = new NYStyleCheesePizza();
        }

        return pizza;
    }
}
