package io.peg.dp.factory.abs.pizza;

/**
 * Created by atom on 2018/11/10.
 */
public class NYStyleCheesePizza extends Pizza {
    public NYStyleCheesePizza() {
        this.name = "NYStyleCheesePizza";
        this.dough = "薄面";
        this.sauce = "沙拉酱";
        this.toppings.add("奶酪");
    }

    public void cut() {
        System.out.println("把披萨切成正方形...");
    }
}
