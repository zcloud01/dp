package io.peg.dp.factory.abs.store;


import io.peg.dp.factory.abs.pizza.Pizza;

/**
 * Created by atom on 2018/11/10.
 */
public abstract class PizzaStore {
    public Pizza orderPizza(String type) {
        Pizza pizza = null;
        pizza = createPizza(type);
        pizza.prepare();
        pizza.bake();
        pizza.cut();
        pizza.box();

        return pizza;
    }

    protected abstract Pizza createPizza(String type);
}
