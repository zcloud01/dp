package io.peg.dp.factory.abs.pizza;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by atom on 2018/11/10.
 */
public abstract class Pizza {
    protected String name;
    protected String dough; // 面团
    protected String sauce; // 酱料
    protected List<String> toppings = new ArrayList<>(); // 佐料

    //-----------------------------------------------------
    // 默认一组实现
    //-----------------------------------------------------
    public void prepare() {
        System.out.println("开始准备: " + this.name);
        System.out.println("和面: " + this.dough);
        System.out.println("添加酱料: " + this.sauce);
        System.out.println("添加佐料: " + this.toppings);
        toppings.forEach(System.out::println);
    }

    public void bake() {
        System.out.println("默认烘焙法...");
    }

    public void cut() {
        System.out.println("默认切片法...");
    }

    public void box() {
        System.out.println("默认装盒法...");
    }

    public String getName() {
        return name;
    }
}
