package io.peg.dp.factory.abs.pizza;

/**
 * Created by atom on 2018/11/10.
 */
public class ChicagoStyleCheesePizza extends Pizza {
    public ChicagoStyleCheesePizza() {
        this.name = "ChicagoStyleCheesePizza";
        this.dough = "厚面";
        this.sauce = "番茄酱";
        this.toppings.add("培根");
    }
}
