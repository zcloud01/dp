package io.peg.dp.factory.abs;

import io.peg.dp.factory.abs.store.ChicagoPizzaStore;
import io.peg.dp.factory.abs.store.NYPizzaStore;
import io.peg.dp.factory.abs.store.PizzaStore;
import io.peg.dp.factory.abs.pizza.Pizza;

/**
 * Created by atom on 2018/11/10.
 */
public class Client {
    public static void main(String[] args) {
        PizzaStore nyStore = new NYPizzaStore();
        PizzaStore chigoStore = new ChicagoPizzaStore();

        Pizza pizza = nyStore.orderPizza("cheese");
        System.out.println("Tom 订购了一个：" + pizza.getName());
        System.out.println("---------------------------------");
        pizza = chigoStore.orderPizza("cheese");
        System.out.println("Jerry 订购了一个：" + pizza.getName());
    }
}
