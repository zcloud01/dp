package io.peg.dp.strategy;

public interface WeaponBehavior {
    void useWeapon();
}
