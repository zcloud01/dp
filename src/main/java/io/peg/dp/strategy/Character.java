package io.peg.dp.strategy;

public class Character {
    protected WeaponBehavior weaponBehavior;

    public void fight() {
        this.weaponBehavior.useWeapon();
    }

    public void setWeaponBehavior(WeaponBehavior weaponBehavior) {
        this.weaponBehavior = weaponBehavior;
    }
}
