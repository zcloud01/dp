package io.peg.dp.proxy.remoteProxy;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Random;

public class GumballMachine extends UnicastRemoteObject implements GumballMachineRemote {
    private String location;
    private Random rd;
    private String state;

    public GumballMachine(String location) throws RemoteException {
        this.location = location;
        this.rd = new Random();
    }

    public String getLocation() {
        return location;
    }

    public int getCount() {
        return rd.nextInt();
    }

    public String getState() {
        return this.state;
    }
}
